###  Photo Album

This program makes a rest call out to https://jsonplaceholder.typicode.com/photos in order to pull a list of photos


##### Building the program
1.  Make sure that Java 1.7+ is installed
2.  Make sure maven is installed
3.  Open a command prompt to the project directory and run "mvn package shade:shade"
4.  The result will be put in your target directory filename: photo-album-1.0-SNAPSHOT.jar

##### Running the program
1.  Open a command prompt to the location of the jar file
2.  Type java -jar photo-album-1.0.jar _albumnumber_

Example: java -jar photo-album-1.0.jar 2

 