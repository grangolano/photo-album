package org.jangolano;

import java.util.Objects;

public class Photo {

    private String name;
    private String id;


    Photo(String name, String id){
        this.name = name;
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return name.equals(photo.name) &&
                id.equals(photo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}
