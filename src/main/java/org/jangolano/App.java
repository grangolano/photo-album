package org.jangolano;

import java.util.ArrayList;

public class App {

    public static void main (String [] args){
        if(args.length!=1){
            System.out.println("invalid input");
        }else{
            try{
                String albumId = args[0];
                Integer.parseInt(albumId);
                RestCaller restCaller = new RestCaller();
                String data = restCaller.getData(albumId);
                Album album = new DataParser().getAlbum(data);
                ArrayList<Photo> photos = album.getPhotos();
                for(Photo photo:photos){
                    System.out.println("["+photo.getId()+"] "+photo.getName());
                }
            }catch(NumberFormatException e){
                System.out.println("invalid input must be a number");
            }
        }
    }
}
