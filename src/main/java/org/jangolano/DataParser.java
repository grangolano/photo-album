package org.jangolano;

import org.json.JSONArray;


public class DataParser {

    public Album getAlbum(String json){
        JSONArray array = new JSONArray(json);
        Album album = new Album();
        for (int i = 0; i< array.length();i++){
            String title = array.getJSONObject(i).getString("title");
            String id =  array.getJSONObject(i).getInt("id")+"";
            Photo photo = new Photo(title, id);
            album.addPhoto(photo);
        }
        return album;
    }
}
