package org.jangolano;

import java.util.ArrayList;

public class Album {

    private ArrayList photos = new ArrayList<Photo>();

    public ArrayList<Photo> getPhotos(){
        return photos;
    }

    public void addPhoto(Photo photo){
        photos.add(photo);
    }
}
