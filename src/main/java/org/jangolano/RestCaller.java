package org.jangolano;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class RestCaller {

    public String getData(String albumId) {
        ClientConfig clientConfig = new DefaultClientConfig();
        Client client = Client.create(clientConfig);
        WebResource webResource = client.resource("https://jsonplaceholder.typicode.com/photos?albumId="+albumId);
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
        return response.getEntity(String.class);
    }

}
