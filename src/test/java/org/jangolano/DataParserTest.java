package org.jangolano;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DataParserTest {

    @Test
    public void getAlbumTest(){

        String json="[\n" +
                "  {\n" +
                "    \"albumId\": 5,\n" +
                "    \"id\": 201,\n" +
                "    \"title\": \"nesciunt dolorum consequatur ullam tempore accusamus debitis sit\",\n" +
                "    \"url\": \"https://via.placeholder.com/600/250289\",\n" +
                "    \"thumbnailUrl\": \"https://via.placeholder.com/150/250289\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"albumId\": 5,\n" +
                "    \"id\": 202,\n" +
                "    \"title\": \"explicabo vel omnis corporis debitis qui qui\",\n" +
                "    \"url\": \"https://via.placeholder.com/600/6a0f83\",\n" +
                "    \"thumbnailUrl\": \"https://via.placeholder.com/150/6a0f83\"\n" +
                "  },]";
        DataParser dataParser = new DataParser();
        Album album =dataParser.getAlbum(json);
        Photo photo = new Photo("nesciunt dolorum consequatur ullam tempore accusamus debitis sit", "201");
        Photo photo1 = new Photo("explicabo vel omnis corporis debitis qui qui", "202");
        assertEquals(photo, album.getPhotos().get(0));
        assertEquals(photo1, album.getPhotos().get(1));

    }
}
