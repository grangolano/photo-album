package org.jangolano;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
public class AlbumTest {

    @Test
    public void testAddAlbum(){
        Album album = new Album();
        Photo photo = new Photo("Test Me", "123");
        album.addPhoto(photo);
        assertEquals(photo, album.getPhotos().get(0));
    }

    @Test
    public void testAddAlbumSeveral(){
        Album album = new Album();
        Photo photo = new Photo("Test Me", "123");
        album.addPhoto(photo);

        Photo photo1 = new Photo("Test Me 1", "456");
        album.addPhoto(photo1);

        Photo photo2 = new Photo("Test Me 2", "789");
        album.addPhoto(photo2);

        Photo photo3 = new Photo("Test Me 3", "101112");
        album.addPhoto(photo3);
        assertEquals(photo, album.getPhotos().get(0));
        assertEquals(photo1, album.getPhotos().get(1));
        assertEquals(photo2, album.getPhotos().get(2));
        assertEquals(photo3, album.getPhotos().get(3));
    }


}
