package org.jangolano;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
public class RestCallerTest {

    @Test
    public void testRestCaller(){
        RestCaller restCaller = new RestCaller();
        assertTrue(restCaller.getData("3").contains("incidunt alias vel enim"));
    }
}
